remote_state {
  backend = "azurerm"
  config = {
    resource_group_name  = "Gregory_M_Projet_Final"
    storage_account_name = "storagebackendmgpjfi"
    container_name       = "tfstate"
    key                  = "dev-test-2.terraform.tfstate"
  }
}
