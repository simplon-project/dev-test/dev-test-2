terraform {
  backend "azurerm" {
    resource_group_name  = "Gregory_M_Projet_Final"
    storage_account_name = "storagebackendmgpjfi"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
  }
}
