#!/bin/bash

# Définir les variables
RESOURCE_GROUP="Gregory_M_Projet_Final"
LOCATION="westeurope"
KEY_VAULT_NAME="keyvaultmgpjfi"
STORAGE_ACCOUNT_NAME="storagebackendmgpjfi"
CONTAINER_NAME="tfstate"

# Créer un groupe de ressources
az group create --name $RESOURCE_GROUP --location $LOCATION

# Créer un Key Vault
az keyvault create --name $KEY_VAULT_NAME --resource-group $RESOURCE_GROUP --location $LOCATION

# Créer un compte de stockage
az storage account create --name $STORAGE_ACCOUNT_NAME --resource-group $RESOURCE_GROUP --location $LOCATION --sku Standard_LRS

# Attendre que le compte de stockage soit créé
echo "Attente de la création du compte de stockage..."
sleep 60

# Récupérer la clé de compte de stockage
STORAGE_ACCOUNT_KEY="$(az storage account keys list --account-name "$STORAGE_ACCOUNT_NAME" --query [0].value -o tsv)"

# Stocker la clé du compte de stockage dans le Key Vault
az keyvault secret set --vault-name "$KEY_VAULT_NAME" --name StorageAccountKey --value "$STORAGE_ACCOUNT_KEY"

# Créer un container
az storage container create --name "$CONTAINER_NAME" --account-name "$STORAGE_ACCOUNT_NAME" --account-key "$STORAGE_ACCOUNT_KEY" --public-access "blob"

echo "Key Vault, compte de stockage et container créés avec succès."

# Créer le fichier backend.tf
echo "Création du fichier backend.tf..."
echo "terraform {
  backend \"azurerm\" {
    resource_group_name  = \"$RESOURCE_GROUP\"
    storage_account_name = \"$STORAGE_ACCOUNT_NAME\"
    container_name       = \"$CONTAINER_NAME\"
    key                  = \"terraform.tfstate\"
  }
}" >backend.tf

echo "Le script a terminé avec succès !"

